package com.sznari.fragmentoldamapsdk;

import android.graphics.PixelFormat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends FragmentActivity implements View.OnClickListener{

    private FragmentA fragmentA;
    private FragmentB fragmentB;
    private FragmentC fragmentC;
    private FragmentTransaction ft;
    private FragmentManager fm;

    private String fragmentAName;
    private String fragmentBName;
    private String fragmentCName;
    private Fragment preFragment;
    private String preFragmentName;

    private boolean firstClickTab2 = true;
    private boolean firstClickTab3 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        setContentView(R.layout.activity_main);

        initView();
        initFragment();
    }

    private void initView() {
        findViewById(R.id.tab1).setOnClickListener(this);
        findViewById(R.id.tab2).setOnClickListener(this);
        findViewById(R.id.tab3).setOnClickListener(this);
    }

    private void initFragment() {
        fragmentA = new FragmentA();
        fragmentB = new FragmentB();
        fragmentC = new FragmentC();
        fragmentAName = fragmentA.getClass().getName();
        fragmentBName = fragmentB.getClass().getName();
        fragmentCName = fragmentC.getClass().getName();

        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        ft.add(R.id.content_container, fragmentA, fragmentAName);
        preFragment = fragmentA;
        preFragmentName = fragmentAName;
        ft.commit();
    }

    @Override
    public void onClick(View view) {

        ft = fm.beginTransaction();

        switch (view.getId()) {
            case R.id.tab1 :
                if (preFragmentName.equals(fragmentAName)) {
                    break;
                } else {
                    ft.detach(preFragment);
                    ft.attach(fragmentA);
                    preFragment = fragmentA;
                    preFragmentName =fragmentAName;
                    ft.commit();
                }
                break;
            case R.id.tab2 :
                if (firstClickTab2) {
                    ft.detach(preFragment);
                    ft.add(R.id.content_container, fragmentB, fragmentBName);
                    preFragment = fragmentB;
                    preFragmentName = fragmentBName;
                    firstClickTab2 = false;
                    ft.commit();
                } else {
                    if (preFragmentName.equals(fragmentBName)) {
                        break;
                    } else {
                        ft.detach(preFragment);
                        ft.attach(fragmentB);
                        preFragment = fragmentB;
                        preFragmentName = fragmentBName;
                        ft.commit();
                    }
                }
                break;
            case R.id.tab3 :
                if (firstClickTab3) {
                    ft.detach(preFragment);
                    ft.add(R.id.content_container, fragmentC, fragmentCName);
                    preFragment = fragmentC;
                    preFragmentName = fragmentCName;
                    firstClickTab3 = false;
                    ft.commit();
                } else {
                    if (preFragmentName.equals(fragmentCName)) {
                        break;
                    } else {
                        ft.detach(preFragment);
                        ft.attach(fragmentC);
                        preFragment = fragmentC;
                        preFragmentName = fragmentCName;
                        ft.commit();
                    }
                }
                break;
        }
    }
}
