package com.sznari.fragmentoldamapsdk;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.CameraUpdate;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.services.core.LatLonPoint;

import butterknife.Bind;
import butterknife.ButterKnife;

//import com.amap.api.navi.INaviInfoCallback;
//import com.amap.api.navi.model.AMapNaviLocation;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentA extends Fragment implements LocationSource, AMapLocationListener {

    @Bind(R.id.amap_map_view)
    MapView mMapView;

    private static final String TAG = "MapFragment";
    private static final int STROKE_COLOR = Color.argb(26, 0, 206, 180);
    private static final int FILL_COLOR = Color.argb(26, 0, 206, 180);

    private View mRoot;

    private AMap mAMap;
    private UiSettings mUiSettings;
    private MyLocationStyle mMyLocationStyle;
    // 定位服务类，提供单次定位、持续定位、地理围栏、最后位置相关功能
    private LocationSource.OnLocationChangedListener mLocationChangedListener;
    private AMapLocationClient mLocationClient;
    // 定位参数设置
    private AMapLocationClientOption mLocationClientOption;
    private boolean isFirstLoc = true; // 第一次定位标识
    private LatLng locatedLL;
    private LatLonPoint mLocatedLatLonPoint;
    private static String locatedCity;


    public FragmentA() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "- - - - - onCreate() called.");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "- - - - - onCreateView() called.");
        if (mRoot == null) {
            mRoot = inflater.inflate(R.layout.fragment_a, container, false);
            ButterKnife.bind(this, mRoot);

            mMapView.onCreate(savedInstanceState);
            initStationMap();

        }else {

            ViewGroup parent = (ViewGroup) mRoot.getParent();
            if (parent != null)
                parent.removeView(mRoot);

        }

        return mRoot;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        Log.d(TAG, "- - - - - onActivityCreated() called.");
        super.onActivityCreated(savedInstanceState);

    }

    public void initStationMap() {

        Log.d(TAG, "- - - - - initStationMap() called.");

        if (mAMap == null) {
            Log.i(TAG, "- - - - - mAmap == null!!!");
            mAMap = mMapView.getMap();
            Log.i(TAG, "- - - - - mAmap created!!!");
        }

        CameraUpdate cameraUpdate = CameraUpdateFactory.zoomTo(12);
        mAMap.moveCamera(cameraUpdate); // 设置地图的默认放大级别

        mUiSettings = mAMap.getUiSettings();
        mAMap.setMapType(AMap.MAP_TYPE_NORMAL);  // 普通

        mAMap.setLocationSource(FragmentA.this); // 设置定位监听，需实现 LocationSource的接口：activate(LocationSource.OnLocationChangedListener var1)和 deactivate()

        mAMap.setMyLocationEnabled(true); // 显示定位层，并且可以触发定位

        mUiSettings.setMyLocationButtonEnabled(true); // 显示定位按钮
        mUiSettings.setCompassEnabled(true);//显示指南针

        mUiSettings.setScaleControlsEnabled(true);//显示比例尺
        mUiSettings.setZoomPosition(AMapOptions.ZOOM_POSITION_RIGHT_CENTER);
        mUiSettings.setLogoPosition(AMapOptions.LOGO_POSITION_BOTTOM_LEFT);//Logo位置

    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {

        Log.d(TAG, "activate() called.");

        mLocationChangedListener = onLocationChangedListener; // 激活定位回调监听器，否则接收不到定位

        if (mLocationClient == null) {
            mLocationClient = new AMapLocationClient(getContext()); // 新建定位客户端实例
            mLocationClientOption = new AMapLocationClientOption(); // 新建定位参数实例
            // 设置定位回调监听
            mLocationClient.setLocationListener(this);

            // 为定位参数实例添加参数
            mLocationClientOption.setLocationMode(AMapLocationClientOption
                    .AMapLocationMode.Hight_Accuracy); // 设置为高精度定位模式
//            mLocationClientOption.setInterval(5000); // 设置定位时间间隔，单位毫秒，最小值2000
            mLocationClientOption.setOnceLocation(true); // 设置只定位一次

            // 为定位客户端实例设置定位参数
            mLocationClient.setLocationOption(mLocationClientOption);


            // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
            // 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
            // 在定位结束后，在合适的生命周期调用onDestroy()方法
            // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
            mLocationClient.startLocation();
        }

        Log.i(TAG, "- - - - - active done!!!");
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {

        if (mLocationChangedListener != null) {
            mLocationChangedListener.onLocationChanged(aMapLocation); // 显示系统小蓝点
        }

        if (aMapLocation != null) {
            if (aMapLocation.getErrorCode() == 0) {

                locatedLL = new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude());
                mLocatedLatLonPoint = new LatLonPoint(
                        aMapLocation.getLatitude(), aMapLocation.getLongitude()
                );

                mAMap.moveCamera(CameraUpdateFactory.zoomTo(11)); // 设置缩放级别

//                // 如果不设标志位（aMapLocation），拖动地图时会不断移动动到当前的位置
                if (isFirstLoc) {
                    mAMap.moveCamera(CameraUpdateFactory.changeLatLng(locatedLL)); // 将地图移动到定位点
                    isFirstLoc = false;
                }

                mLocationChangedListener.onLocationChanged(aMapLocation); //点击定位按钮，将地图的中心移动到定位点

                StringBuffer cityBuffer = new StringBuffer();
                cityBuffer.append(aMapLocation.getCity());
                locatedCity = cityBuffer.toString(); // 将定位得到的城市赋值给 city
                Log.d(TAG, locatedCity);

            } else {
                //显示错误信息：ErrCode是错误码，errInfo是错误信息，详见错误码表。
                Log.e("AmapError", "location Error, ErrCode:"
                        + aMapLocation.getErrorCode() + ", errInfo:"
                        + aMapLocation.getErrorInfo());
            }

        }
    }

    @Override
    public void deactivate() {

        Log.d(TAG, "- - - - - deactivate() called." );

        mLocationChangedListener = null;
        if (mLocationClient != null) {
            mLocationClient.stopLocation();
            mLocationClient.onDestroy();
        }
        mLocationClient = null;
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();

        Log.d(TAG, "- - - - -mMapView.onResume() was called.");
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        Log.d(TAG, "- - - - -mMapView.onPause() was called.");
        deactivate(); // 停止定位，方法必须重写
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
        Log.d(TAG, "- - - - - mMapView.onSaveInstanceState(outState) was called.");
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        Log.d(TAG, "- - - - - mMapView.onDestroy() was called.");
    }
}
